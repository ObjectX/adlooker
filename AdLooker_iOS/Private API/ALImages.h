//
//  ALImages.h
//  AdLooker_iOS
//
//  Created by Gagik Kyurkchyan on 8/13/14.
//  Copyright (c) 2014 com.objectx. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@interface ALImages : NSObject

+(UIImage*)getAdLookerIcon;
+(UIImage*)getCloseButton;
+(UIImage*)getCloseButtonHover;
@end
