//
//  ALAdRequest.h
//  AdLooker_iOS
//
//  Created by Gagik Kyurkchyan on 8/1/14.
//  Copyright (c) 2014 com.objectx. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ApiConstants.h"

@interface ALAdRequest : NSObject

+(ALAdRequest *)adRequestWithAdType:(ALAdType)adType;

@property (nonatomic) float time;
@property (nonatomic) ALAdType adType;
@property (nonatomic, strong) NSString * requestKey;

-(NSData *)convertToJsonData;
-(NSData *)convertToJsonString;
@end
