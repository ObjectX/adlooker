//
//  ALAdRequest.m
//  AdLooker_iOS
//
//  Created by Gagik Kyurkchyan on 8/1/14.
//  Copyright (c) 2014 com.objectx. All rights reserved.
//

#import "ALAdRequest.h"

#define kTimeZoneKey @"timezone"
#define kTimeKey @"time"
#define kAdTypeKey @"adType"
#define kRequestKey @"extra_key"
@implementation ALAdRequest

+(ALAdRequest *)adRequestWithAdType:(ALAdType)adType
{
    ALAdRequest * request = [[ALAdRequest alloc] init];
    
    request.adType = adType;
    NSCalendar * calendar = [NSCalendar currentCalendar];
    NSDate * date = NSDate.date;
    NSDateComponents * dateComponents = [calendar components:NSCalendarUnitHour | NSCalendarUnitTimeZone | NSCalendarUnitMinute fromDate:date];
    request.time = dateComponents.hour + (dateComponents.minute - (float)dateComponents.timeZone.secondsFromGMT / 60)/60;

    return request;
}

-(NSData *)convertToJsonData
{
    NSNumber * time = [NSNumber numberWithFloat:self.time];
    NSString * adType = self.adType == kAdTypeBanner ? kAdTypeBannerString : kAdTypeInterestitialString;
    NSDictionary * dataDict = @{ kTimeKey : time, kAdTypeKey : adType, kRequestKey : self.requestKey};
    NSError * error = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dataDict options:0 error:&error];
    if(!error)
        return jsonData;
    else
    {
        NSString *JSONString = [[NSString alloc] initWithBytes:[jsonData bytes] length:[jsonData length] encoding:NSUTF8StringEncoding];
        DLog(@"Could not convert ALDeviceInfo to json. %@", JSONString);
        return nil;
    }
}

-(NSString *)convertToJsonString
{
    NSData * jsonData = self.convertToJsonData;
    if(jsonData != nil)
        return [[NSString alloc] initWithBytes:[jsonData bytes] length:[jsonData length] encoding:NSUTF8StringEncoding];
    return nil;
}
@end
