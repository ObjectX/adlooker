//
//  ALAdObject.m
//  AdLooker_iOS
//
//  Created by Gagik Kyurkchyan on 7/29/14.
//  Copyright (c) 2014 com.objectx. All rights reserved.
//

#import "ALAdvertisement.h"

#define kTargetUrlKey @"href"
#define kImageUrlKey @"img"
#define kAdTypeKey @"adType"

@implementation ALAdvertisement

+(ALAdvertisement *)parseFromJsonData:(NSData *)jsonData
{
    NSError *error = nil;
    NSDictionary *dataDict = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:&error];
    
    if(jsonData == nil)
    {
        NSString *JSONString = [[NSString alloc] initWithBytes:[jsonData bytes] length:[jsonData length] encoding:NSUTF8StringEncoding];
        DLog(@"Could not parse jsonData into ALAdvertisement object. %@ %@", error, JSONString);
        return nil;
    }
    
    ALAdvertisement * ad = [ALAdvertisement new];
    ad.targetUrl = [dataDict objectForKey:kTargetUrlKey];
    ad.imageUrl = [dataDict objectForKey:kImageUrlKey];
    NSString * adType = [dataDict objectForKey:kAdTypeKey];
    ad.adType =  [adType isEqualToString:kAdTypeBannerString] ? kAdTypeBanner : kAdTypeInterestitial;
    return ad;
}

+(ALAdvertisement *)parseFromJsonString:(NSString *)jsonString
{
    NSData *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    return [ALAdvertisement parseFromJsonData:jsonData];
}

@end
