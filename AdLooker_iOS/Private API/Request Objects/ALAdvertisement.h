//
//  ALAdObject.h
//  AdLooker_iOS
//
//  Created by Gagik Kyurkchyan on 7/29/14.
//  Copyright (c) 2014 com.objectx. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "ApiConstants.h"

@interface ALAdvertisement : NSObject

@property (strong, nonatomic) UIImage * adImage;
@property (strong, nonatomic) NSString * targetUrl;
@property (strong, nonatomic) NSString * imageUrl;
@property (nonatomic) ALAdType adType;

+(ALAdvertisement *)parseFromJsonData:(NSData *)jsonData;
+(ALAdvertisement *)parseFromJsonString:(NSString *)jsonString;
@end
