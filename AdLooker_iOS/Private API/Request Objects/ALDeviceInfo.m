//
//  ALDeviceInfo.m
//  AdLooker_iOS
//
//  Created by Gagik Kyurkchyan on 7/31/14.
//  Copyright (c) 2014 com.objectx. All rights reserved.
//

#import "ALDeviceInfo.h"
#import <UIKit/UIKit.h>
@import AdSupport;

#define kModelKey @"Model"
#define kSettingsKey @"Settings"
#define kOsNameKey @"DeviceSystem"
#define kDeviceTypeKey @"DeviceType"
#define kManufacturerKey @"Device"
#define kDeviceInfoKey @"DeviceOptions"
#define kAdLookerAppIdJsonKey @"idAdspace"
#define kOsVersionMinor @"DeviceMinor"
#define kOsVersionMajor @"DeviceMajor"
#define kAdvertisingId @"deviceId"

@implementation ALDeviceInfo

#pragma mark - Initialization
+(ALDeviceInfo *)getDeviceInfo
{
    ALDeviceInfo * deviceInfo = [[ALDeviceInfo alloc] init];
    UIDevice * device = [UIDevice currentDevice];
    deviceInfo.model = device.model;
    deviceInfo.settings = device.model;
    deviceInfo.manufacturer = @"Apple";
    deviceInfo.osName = @"iOS";
    deviceInfo.advertisingId = [[[ASIdentifierManager sharedManager] advertisingIdentifier] UUIDString];
    
    NSString * version = device.systemVersion;
    NSRange range = [version rangeOfString:@"."];
    if (range.location != NSNotFound) {
        deviceInfo.osVersionMajor = [version substringWithRange:NSMakeRange(0, range.location)];
        deviceInfo.osVersionMinor = [version substringFromIndex:range.location+1];
    }
    else
    {
        deviceInfo.osVersionMajor = version;
        deviceInfo.osVersionMinor = @"";
    }
   
    deviceInfo.type = device.userInterfaceIdiom == UIUserInterfaceIdiomPad ? @"tablet" : @"phone";
    return deviceInfo;
}

#pragma mark - Properties
-(NSString *)AdLookerAppId
{
    if(_AdLookerAppId == nil)
    {
        _AdLookerAppId = [self getAppId];
        if(_AdLookerAppId == nil)
        {
            NSLog(@"AdLooker App id is not specified. Please specify it in your Info.plist file using %@ key", kAdLookerAppIdKey);
        }
    }
    return _AdLookerAppId;
}

#pragma mark - Public API
-(NSData *)convertToJsonData
{
    NSDictionary * deviceInfoDict = @{ kManufacturerKey : self.manufacturer,
                                       kSettingsKey : self.settings,
                                       kModelKey : self.model
                                       };
    NSDictionary * dataDict = @{
                                kAdLookerAppIdJsonKey : self.AdLookerAppId,
                                kOsNameKey : self.osName,
                                kOsVersionMajor : self.osVersionMajor,
                                kOsVersionMinor : self.osVersionMinor,
                                kDeviceTypeKey : self.type,
                                kAdvertisingId : self.advertisingId,
                                kDeviceInfoKey : deviceInfoDict
                                };
    NSError * error = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dataDict options:0 error:&error];
    if(!error)
        return jsonData;
    else
    {
        NSString *JSONString = [[NSString alloc] initWithBytes:[jsonData bytes] length:[jsonData length] encoding:NSUTF8StringEncoding];
        DLog(@"Could not convert ALDeviceInfo to json. %@", JSONString);
        return nil;
    }
}

-(NSString *)convertToJsonString
{
    NSData * jsonData = self.convertToJsonData;
    if(jsonData != nil)
        return [[NSString alloc] initWithBytes:[jsonData bytes] length:[jsonData length] encoding:NSUTF8StringEncoding];
    return nil;
}

#pragma mark - Utility Methods
-(NSString *)getAppId
{
    return [[NSBundle mainBundle] objectForInfoDictionaryKey:kAdLookerAppIdKey];
}
@end
