//
//  ALDeviceInfo.h
//  AdLooker_iOS
//
//  Created by Gagik Kyurkchyan on 7/31/14.
//  Copyright (c) 2014 com.objectx. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ApiConstants.h"

@interface ALDeviceInfo : NSObject

+(ALDeviceInfo*)getDeviceInfo;
-(NSData *)convertToJsonData;
-(NSData *)convertToJsonString;

@property (strong, nonatomic) NSString * model;
@property (strong, nonatomic) NSString * manufacturer;
@property (strong, nonatomic) NSString * settings;
@property (strong, nonatomic) NSString * osName;
@property (strong, nonatomic) NSString * osVersionMinor;
@property (strong, nonatomic) NSString * osVersionMajor;
@property (strong, nonatomic) NSString * type;
@property (strong, nonatomic) NSString * AdLookerAppId;
@property (strong, nonatomic) NSString * advertisingId;
@end
