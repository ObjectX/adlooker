//
//  ALBannerView.m
//  AdLooker_iOS
//
//  Created by Gagik Kyurkchyan on 7/25/14.
//  Copyright (c) 2014 com.objectx. All rights reserved.
//

#import "ALInterestitialView.h"
#import "ALAdManager.h"
#import "ALInterestitial.h"

#define kWrongInitializationError @"ALBannerView instance can only be initialized using initWithParentViewController:atPosition: constructor."
#define kNilParentVCError @"Parent view controller can not be null."
#define kNilWindowError @"Parent view controller's view is not added to window hierarchy, make sure you call initWithParentViewController:atPosition: in viewDidAppear of parent view controller."

@interface ALInterestitialView() <ALAdListener>

@property (nonatomic, strong) ALAdvertisement * advertisment;
@property (nonatomic, strong) UIImageView * imageView;
-(void)initializeTapGesture;
-(void)interestitialAdTapped:(UITapGestureRecognizer *)tapGesture;

@end

@implementation ALInterestitialView

#pragma mark - initialization

- (id)init
{
    if(self = [super initWithFrame:CGRectZero])
    {
        [self initializeImageView];
        [self initializeTapGesture];
    }
    return self;
}

-(void)dealloc
{
    [[ALAdManager instance] removeAdListener:self forAdType:kAdTypeInterestitial];
}

-(void)hide
{
    [ALInterestitial closeInterestitial];
}


#pragma mark - View Lifecycle management

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
    [super drawRect:rect];
    [[ALAdManager instance] addAdListener:self forAdType:kAdTypeInterestitial];
}

#pragma mark - ALAdListener
-(void)didReloadAd:(ALAdvertisement *)newAd
{
    self.advertisment = newAd;
    self.imageView.image = newAd.adImage;
}

#pragma mark - Utility methods

-(void)initializeImageView
{
    self.imageView = [[UIImageView alloc] initWithFrame:self.bounds];
    self.imageView.contentMode = UIViewContentModeScaleAspectFit;
    self.imageView.backgroundColor = [UIColor clearColor];
    [super addSubview:self.imageView];
    self.imageView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    self.imageView.frame = self.bounds;
}

-(void)initializeTapGesture
{
    UITapGestureRecognizer * tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(interestitialAdTapped:)];
    [self addGestureRecognizer:tapGesture];
}


-(void)interestitialAdTapped:(UITapGestureRecognizer *)tapGesture
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:self.advertisment.targetUrl]];
}


@end
