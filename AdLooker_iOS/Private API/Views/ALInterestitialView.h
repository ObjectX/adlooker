//
//  ALInterestitialView.h
//  AdLooker_iOS
//
//  Created by Gagik Kyurkchyan on 8/3/14.
//  Copyright (c) 2014 com.objectx. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ALInterestitialView : UIView
- (id)init;
@end
