//
//  Constants.h
//  AdLooker_iOS
//
//  Created by Gagik Kyurkchyan on 7/25/14.
//  Copyright (c) 2014 com.objectx. All rights reserved.
//
#import <UIKit/UIKit.h>
#ifndef AdLooker_API_Constants_h
#define AdLooker_API_Constants_h

/////////////////////////////////////////////////////////////////////////////////
//////////////////////////////Enumerations///////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
typedef NS_ENUM(NSInteger, ALAdType)
{
    kAdTypeBanner,
    kAdTypeInterestitial
};

/////////////////////////////////////////////////////////////////////////////////
/////////////User Interface Constants////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
#define BANNER_IPHONE_PORTRAIT_HEIGHT 50
#define BANNER_IPHONE_LANDSCAPE_HEIGHT 32
#define BANNER_IPAD_PORTRAIT_HEIGHT 66
#define BANNER_IPAD_LANDSCAPE_HEIGHT 66

/////////////////////////////////////////////////////////////////////////////////
////////////////////////General Constants////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

#define kAdLookerAppIdKey @"ADLOOKER_APP_KEY"
#define kBaseApiUrlKey @"ADLOOKER_ROOT_URL"

/////////////////////////////////////////////////////////////////////////////////
////////////////////////API Constants////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

//Urls
#define kAuthenticationAction @"startsesion"
#define kAdsAction @"advertising"
#define kAuthenticationUrl @"/inapp/startsession"
#define kAdsUrl @"/inapp/advertising"
#define kRegisterNewInstallUrl @"/inapp/registerinstall/"

//Timeouts
#define kRequestTimeoutSeconds 30
#define kAdRequestTimeoutSeconds 10

//HTTP Header names
#define kAuthorizationHTTPHeader @"Authorization"

//Misc
#define kAdTypeBannerString @"mobileBanner"
#define kAdTypeInterestitialString @"mobileInterestitial"

static BOOL IS_BELOW_IOS_7()
{
    static BOOL answer;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        answer = [[[UIDevice currentDevice] systemVersion] floatValue] <= 7.0;
    });
    return answer;
}

static BOOL IS_BELOW_IOS_6()
{
    static BOOL answer;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        answer = [[[UIDevice currentDevice] systemVersion] floatValue] <= 6.0;
    });
    return answer;
}

static BOOL IS_IOS_8_OR_HIGHER()
{
    static BOOL answer;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        answer = floor([[[UIDevice currentDevice] systemVersion] floatValue]) >= 8.0;
    });
    return answer;
}

#ifndef DLog
#ifdef DEBUG
#define DLog(_format_, ...) NSLog(_format_, ## __VA_ARGS__)
#else
#define DLog(_format_, ...)
#endif
#endif

#endif
