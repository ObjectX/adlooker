//
//  NSURL+Additions.h
//  AdLooker_iOS
//
//  Created by Gagik Kyurkchyan on 7/29/14.
//  Copyright (c) 2014 com.objectx. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSURL (Additions)

-(NSURL *)URLByAddingQueryStringParameters:(NSDictionary *)parameters;

@end
