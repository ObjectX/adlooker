//
//  NSString+MD5.h
//  AdLooker_iOS
//
//  Created by Gagik Kyurkchyan on 8/3/14.
//  Copyright (c) 2014 com.objectx. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (MD5)
- (NSString*)MD5;
@end
