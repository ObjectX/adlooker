//
//  NSURL+Additions.m
//  AdLooker_iOS
//
//  Created by Gagik Kyurkchyan on 7/29/14.
//  Copyright (c) 2014 com.objectx. All rights reserved.
//

#import "NSURL+Additions.h"

@implementation NSURL (Additions)
-(NSURL*)URLByAddingQueryStringParameters:(NSDictionary *)parameters
{
    if(parameters.count == 0)
        return self;
    NSMutableArray * queryStringArray = [[NSMutableArray alloc] initWithCapacity:parameters.count];
    for (NSString * key in parameters) {
        NSString * value = [parameters objectForKey:key];
        NSString * valueEncoded = [value stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSString * keyEncoded = [key stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        [queryStringArray addObject:[NSString stringWithFormat:@"%@=%@", keyEncoded, valueEncoded]];
    }
    NSString * queryString = [queryStringArray componentsJoinedByString:@"&"];
    NSString *URLString = [[NSString alloc] initWithFormat:@"%@%@%@", [self absoluteString],
                           [self query] ? @"&" : @"?", queryString];
    NSURL *theURL = [NSURL URLWithString:URLString];
    return theURL;
}
@end
