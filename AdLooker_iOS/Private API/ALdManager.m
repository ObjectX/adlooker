//
//  AdLoader.m
//  AdLooker_iOS
//
//  Created by Gagik Kyurkchyan on 7/29/14.
//  Copyright (c) 2014 com.objectx. All rights reserved.
//

#import "ALAdManager.h"
#import "NSURL+Additions.h"
#import "Constants.h"
#import "ApiConstants.h"
#import "ALDeviceInfo.h"
#import "ALAdRequest.h"
#import "NSString+MD5.h"
#import "ALBannerView.h"
#import "ALInterestitialView.h"
@import AdSupport;

typedef NS_ENUM(NSInteger, ALAuthenticationResult)
{
    kAuthenticationUnknown,
    kAuthenticationSuccessful,
    kAuthenticationFailed,
    kAuthenticationWrongAppId
};

#define kSuccessfulResultCode 200
#define kAuthenticationFailedResultCode 404
#define kAuthenticationWrongAppId 504
#define kTooFastAdLoading 402
#define kNoAdToLoad 300
#define kAuthorizationKey @"authorization"
#define kIsDeviceRegisteredKey @"com.AdLooker.ios:IsDeviceRegistered"

typedef void (^LoadAdCompletion) (ALAdvertisement *, NSError *);
typedef void (^LoadAdImageCompletion) (UIImage *, NSError*);
typedef void (^AuthenticationCompletion) (ALAuthenticationResult, NSError*);
typedef void (^RegisterNewInstallCallback)(BOOL, NSError *);
typedef void (^loadAd)(ALAdType adType);

@interface WeakListener : NSObject
@property (nonatomic, weak) id<ALAdListener> weakListener;
+(WeakListener *)getWeakListener:(id<ALAdListener>)listener;
@end

@implementation WeakListener
+(WeakListener *)getWeakListener:(id<ALAdListener>)listener
{
    WeakListener * weakListener = [[WeakListener alloc] init];
    weakListener.weakListener = listener;
    return weakListener;
}

-(BOOL)isEqual:(id)other
{
    if (!other || ![other isKindOfClass:[self class]])
        return NO;
    WeakListener * listener = (WeakListener*)other;
    return [self.weakListener isEqual:listener.weakListener];
}
@end


@interface ALAdManager()
{
    NSMutableData * _responseData;
}


//
//Ad loading
//
//creates a new thread (if it doesn't exist or was stopped) which will request ads by and suspend thread until specified timeout expires and start over again
-(void)startLoadingAdIfNeeded:(ALAdType)adType;
//stops the ad loading threads
-(void)stopLoadingAdIfNeeded:(ALAdType)adType;
//the method associated with thread which actually does the job of previous two functions
-(void)adLoader:(NSNumber *)adType;

//Making HTTP requests
-(void)authenticateAsync:(AuthenticationCompletion)callback;
-(void)loadAdWithTypeAsync:(ALAdType)adType  completionCallback:(LoadAdCompletion)callback;
-(void)loadAdImageAsyncForUrl:(NSString *)imageUrl completionCallback:(LoadAdImageCompletion)callback;
-(NSString *)getSessionIdFromResponseBody:(NSData *)body;
-(void)registerNewInstallAsync:(RegisterNewInstallCallback)callback;
-(void)registerNewInstallIfNeeded;
@property (strong, nonatomic) NSString * baseApiUrl;
//Utility methods
-(void)notifyListenersAdLoadedForAdType:(ALAdType)adType loadedAd:(ALAdvertisement *)ad;
-(NSString *)getRequestKey;
//Validation
-(BOOL)validateBannerListeners;
-(BOOL)validateInterestitialListeners;
-(BOOL)validateBannerListener:(id<ALAdListener>)listener;
-(BOOL)validateInterestitialListener:(id<ALAdListener>)listener;
-(BOOL)validateCommon:(UIView *)adView;

//Ad loading queue
@property (strong, nonatomic) NSOperationQueue * adLoadingOperationQueue;

//Cached ads
@property (strong, nonatomic) ALAdvertisement * currentBanner;
@property (strong, nonatomic) ALAdvertisement * currentInterestitial;

//Listener sets
@property (strong, nonatomic) NSMutableSet * bannerListeners;
@property (strong, nonatomic) NSMutableSet * interestitialListeners;

//Ad loading threads
@property (strong, nonatomic) NSThread * banerUpdateThread;
@property (strong, nonatomic) NSThread * interestitialUpdateThread;
@property (strong, nonatomic) NSObject * interestitialLocker;
@property (strong, nonatomic) NSObject * registerNewInstallLocker;
@property (nonatomic) BOOL isRegisterNewInstallInProgress;
@property (strong, nonatomic) NSObject * bannerLocker;

//Ad loading thread killers
@property (nonatomic) BOOL stopBannerUpdateThread;
@property (nonatomic) BOOL stopInterestitialUpdateThread;

//Session related variables
@property (strong, nonatomic) NSString * sessionKey;
@property (nonatomic) NSInteger requestCount;
@property (strong, nonatomic) ALDeviceInfo * deviceInfo;
@property (nonatomic) double lastBannerRequestTime;
@property (nonatomic) double lastInterestitialRequestTime;
@property (nonatomic) int isDeviceRegistered;

@end

@implementation ALAdManager

#pragma mark - Initialization

+ (id)instance {
    static ALAdManager *adManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        adManager = [[self alloc] init];
    });
    return adManager;
}

- (id)init {
    if (self = [super init]) {
        self.adLoadingOperationQueue = [[NSOperationQueue alloc] init];
        //Set max concurrent operation count to one not to allow requesting multiple ads at one time
        //This way ad requests will be sequential
        self.adLoadingOperationQueue.maxConcurrentOperationCount = 1;
        self.interestitialListeners = [NSMutableSet new];
        self.bannerListeners = [NSMutableSet new];
        self.lastBannerRequestTime = 0;
        self.lastInterestitialRequestTime = 0;
        self.requestCount = 1;
        self.interestitialLocker = [NSObject new];
        self.bannerLocker = [NSObject new];
        self.isDeviceRegistered = -1;
    }
    return self;
}

- (void)dealloc {
    // Should never be called, but just here for clarity really.
}

#pragma mark - property accessors
-(ALDeviceInfo *)deviceInfo
{
    if(_deviceInfo == nil)
    {
        _deviceInfo = [ALDeviceInfo getDeviceInfo];
    }
    return _deviceInfo;
}

-(NSString *)baseApiUrl
{
    if(_baseApiUrl == nil)
    {
        _baseApiUrl = [[NSBundle mainBundle] objectForInfoDictionaryKey:kBaseApiUrlKey];
    }
    return _baseApiUrl;
    
}


#pragma mark - Public API implementation

-(void)addAdListener:(id<ALAdListener>)adListener forAdType:(ALAdType)adType
{
    NSObject * locker = adType == kAdTypeBanner ? self.bannerLocker : self.interestitialLocker;
    @synchronized(locker)
    {
        //Validate before insertion, and do not insert if invalid
        if(!( adType == kAdTypeBanner ? [self validateBannerListener:adListener] : [self validateInterestitialListener:adListener]))
            return;
        NSMutableSet * listeners = nil;
        ALAdvertisement * currentAd = nil;
        if(adType == kAdTypeBanner)
        {
            listeners = self.bannerListeners;
            currentAd = self.currentBanner;
        }
        else
        {
            listeners = self.interestitialListeners;
            currentAd = self.currentInterestitial;
        }
        
        if(currentAd != nil)
        {
            [adListener didReloadAd:currentAd];
        }
        WeakListener * weakListener = [WeakListener getWeakListener:adListener];
        if(![listeners containsObject:weakListener])
        {
            [listeners addObject:weakListener];
        }
        [self startLoadingAdIfNeeded:adType];
    }
}

-(void)removeAdListener:(id<ALAdListener>)adListener forAdType:(ALAdType)adType
{
    NSObject * locker = adType == kAdTypeBanner ? self.bannerLocker : self.interestitialLocker;
    @synchronized(locker)
    {
        NSMutableSet * listeners = nil;
        ALAdvertisement * currentAd = nil;
        if(adType == kAdTypeBanner)
        {
            listeners = self.bannerListeners;
            currentAd = self.currentBanner;
        }
        else
        {
            listeners = self.interestitialListeners;
            currentAd = self.currentInterestitial;
        }
        
        if(currentAd != nil)
        {
            [adListener didReloadAd:currentAd];
        }
        NSValue * weakReference = [NSValue valueWithNonretainedObject:adListener];
        [listeners removeObject:weakReference];
        [self stopLoadingAdIfNeeded:adType];
    }
}

#pragma mark - Private API implementation

-(void)startLoadingAdIfNeeded:(ALAdType)adType
{
    NSThread * loaderThread = nil;
    if (adType == kAdTypeBanner) {
        self.stopBannerUpdateThread = false;
        loaderThread = self.banerUpdateThread;
    }
    else
    {
        self.stopInterestitialUpdateThread = false;
        loaderThread = self.interestitialUpdateThread;
    }
    
    if(loaderThread == nil || !loaderThread.isExecuting)
    {
        loaderThread = [[NSThread alloc] initWithTarget:self selector:@selector(adLoader:) object:[NSNumber numberWithInteger:adType]];
        if (adType == kAdTypeBanner) {
            self.banerUpdateThread = loaderThread;
        }
        else
        {
            self.interestitialUpdateThread = loaderThread;
        }
        [loaderThread start];
    }
}

-(void)stopLoadingAdIfNeeded:(ALAdType)adType
{
    NSMutableSet * listeners = nil;
    BOOL *stopLoadingThread;
    if(adType == kAdTypeBanner)
    {
        listeners = self.bannerListeners;
        stopLoadingThread = &_stopBannerUpdateThread;
    }
    else
    {
        listeners = self.interestitialListeners;
        stopLoadingThread = &_stopInterestitialUpdateThread;
    }
    if(listeners.count == 0)
    {
        *stopLoadingThread = true;
    }
}

-(void)adLoader:(NSNumber *)adTypeObject
{
    ALAdType adType = adTypeObject.integerValue;
    while (!(adType == kAdTypeBanner ? self.stopBannerUpdateThread : self.stopInterestitialUpdateThread)) {
        if((adType == kAdTypeBanner ? [self validateBannerListeners] : [self validateInterestitialListeners]))
        {
            __weak typeof(self) weakSelf = self;
            
            loadAd load = ^(ALAdType adType) {
                [weakSelf loadAdWithTypeAsync:adType completionCallback:^(ALAdvertisement * ad, NSError * error) {
                    [weakSelf registerNewInstallIfNeeded];
                    //If returned ad is not nil request ad image
                    if (ad != nil) {
                        [weakSelf loadAdImageAsyncForUrl:ad.imageUrl completionCallback:^(UIImage * image, NSError * error) {
                            if (image != nil) {
                                //If image is not nil notify listeners that new ad is loaded
                                ad.adImage = image;
                                [weakSelf notifyListenersAdLoadedForAdType:adType loadedAd:ad];
                            }
                            else
                            {
                                adType == kAdTypeBanner ? (self.currentBanner = nil) : (self.currentInterestitial = nil);
                                [weakSelf notifyListenersAdLoadedForAdType:adType loadedAd:nil];
                            }
                        }];
                    }
                    else
                    {
                        adType == kAdTypeBanner ? (self.currentBanner = nil) : (self.currentInterestitial = nil);
                        [weakSelf notifyListenersAdLoadedForAdType:adType loadedAd:nil];
                    }
                }];
            };
            //If session key is nil we need to authenticate first
            if(self.sessionKey == nil)
            {
                [self authenticateAsync:^(ALAuthenticationResult result, NSError * error) {
                    if(result == kAuthenticationSuccessful)
                    {
                        load(adType);
                    }
                    else
                    {
                        adType == kAdTypeBanner ? (self.currentBanner = nil) : (self.currentInterestitial = nil);
                        [weakSelf notifyListenersAdLoadedForAdType:adType loadedAd:nil];
                    }
                }];
            }
            else
            {
                load(adType);
            }
        }
        [NSThread sleepForTimeInterval:kAdRequestTimeoutSeconds];
        double diff = [NSDate timeIntervalSinceReferenceDate] - (adType == kAdTypeBanner ? self.lastBannerRequestTime : self.lastInterestitialRequestTime);
        if(diff < kAdRequestTimeoutSeconds)
            [NSThread sleepForTimeInterval:kAdRequestTimeoutSeconds - diff];
    }
}

#pragma mark - Making HTTP requests

-(void)authenticateAsync:(AuthenticationCompletion)callback
{
    //Create ads url
    NSURL * authUrl = [[NSURL URLWithString:self.baseApiUrl] URLByAppendingPathComponent:kAuthenticationUrl];
    //Create request
    NSMutableURLRequest * authRequest = [NSMutableURLRequest requestWithURL:authUrl cachePolicy:NSURLCacheStorageNotAllowed timeoutInterval:kRequestTimeoutSeconds];
    [authRequest setHTTPMethod:@"POST"];
    //Get device info and set as body of request
    NSData * jsonDeviceInto = [self.deviceInfo convertToJsonData];
    [authRequest setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [authRequest setValue:@"application/json; charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
    [authRequest setHTTPBody:jsonDeviceInto];
    //Perform request
    __weak typeof(self) weakSelf = self;
    [NSURLConnection sendAsynchronousRequest:authRequest queue:[NSOperationQueue new] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        ALAuthenticationResult result = kAuthenticationUnknown;
        NSError * error = nil;
        if(!connectionError)
        {
            //Convert to http response to get the status code
            NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
            //If successful then get the session key return inside the body
            if(httpResponse.statusCode == kSuccessfulResultCode)
            {
                weakSelf.sessionKey = [self getSessionIdFromResponseBody:data];
                weakSelf.requestCount = 1;
                result = kAuthenticationSuccessful;
            }
            //If the app ID was
            else if(httpResponse.statusCode == kAuthenticationWrongAppId )
            {
                result = kAuthenticationWrongAppId;
                NSLog(@"The specified appId '%@' is wrong. Please specify correct AdLooker app id in Info.plist file using %@ key", weakSelf.deviceInfo.AdLookerAppId, kAdLookerAppIdKey);
            }
            //General failure
            else
            {
                result = kAuthenticationFailed;
                DLog(@"Error occurred when authenticating to backend. HTTP Status Code - %ld", (long)httpResponse.statusCode);
            }
        }
        else
        {
            result = kAuthenticationFailed;
            error = connectionError;
            DLog(@"Error occurred when authenticating to backend. %@",connectionError);
        }
        dispatch_sync(dispatch_get_main_queue(), ^{
            callback(result, error);
        });
    }];
}

-(void)loadAdWithTypeAsync:(ALAdType)adType  completionCallback:(LoadAdCompletion)callback
{
    //Get the request url
    NSURL * adsUrl = [[NSURL URLWithString:self.baseApiUrl] URLByAppendingPathComponent:kAdsUrl];
    //Create the request
    NSMutableURLRequest * adRequest = [NSMutableURLRequest requestWithURL:adsUrl cachePolicy:NSURLCacheStorageNotAllowed timeoutInterval:kRequestTimeoutSeconds];
    [adRequest setHTTPMethod:@"POST"];
    //Add the session key for Authorization header
    NSString * key = [self getRequestKey];
    [adRequest addValue:self.sessionKey forHTTPHeaderField:kAuthorizationHTTPHeader];
    //Create the ADRequest json and put it inside the request body
    ALAdRequest * request = [ALAdRequest adRequestWithAdType:adType];
    request.requestKey = key;
    NSData * jsonRequest = [request convertToJsonData];
    [adRequest setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [adRequest setValue:@"application/json; charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
    [adRequest setHTTPBody:jsonRequest];
    //Make the request
    __weak typeof(self) weakSelf = self;
    [NSURLConnection sendAsynchronousRequest:adRequest queue:[NSOperationQueue new] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        ALAdvertisement * ad = nil;
        NSError * error = nil;
        if(!connectionError)
        {
            NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
            ///If successful then body contains the ad data with json
            if(httpResponse.statusCode == kSuccessfulResultCode)
            {
                ad = [ALAdvertisement parseFromJsonData:data];
                if(adType == kAdTypeBanner)
                {
                    self.currentBanner = ad;
                }
                else
                {
                    self.currentInterestitial = ad;
                }
                weakSelf.requestCount++;
            }
            else if(httpResponse.statusCode == kNoAdToLoad)
            {
                DLog(@"There are no ads to load.");
            }
            else if(httpResponse.statusCode == kTooFastAdLoading)
            {
                DLog(@"Failed to load ad from backend because ads are being loaded too fast, slow down the ad loading timeout.");
//                weakSelf.requestCount++;
            }
            //This means either the session has been expired or has not been set, in any case we need to nullify the session key, because it's invalid
            else if(httpResponse.statusCode == kAuthenticationFailedResultCode)
            {
                DLog(@"Failed to load ad from backend, The session was expired.");
                weakSelf.sessionKey = nil;
            }
            //General failure, return null result
            else
            {
                DLog(@"Failed to load ad from backend. HTTP status code - %ld", (long)httpResponse.statusCode);
            }
        }
        else
        {
            DLog(@"Error occurred when loading ad from backend %@",connectionError);
            error = connectionError;
        }
        dispatch_sync(dispatch_get_main_queue(), ^{
            callback(ad, error);
        });
    }];
}

-(void)loadAdImageAsyncForUrl:(NSString *)imageUrl completionCallback:(LoadAdImageCompletion)callback
{
    //Create the request
    NSURL * url = [NSURL URLWithString:imageUrl];
    NSMutableURLRequest * adImageRequest = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLCacheStorageNotAllowed timeoutInterval:kRequestTimeoutSeconds];
    //Add the session key for Authorization header
    [adImageRequest addValue:self.sessionKey forHTTPHeaderField:kAuthorizationHTTPHeader];
    //Make the request
    __weak typeof(self) weakSelf = self;
    [NSURLConnection sendAsynchronousRequest:adImageRequest queue:self.adLoadingOperationQueue completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        UIImage * image = nil;
        NSError * error = nil;
        if(!connectionError)
        {
            NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
            //If successful body contains the image
            if(httpResponse.statusCode == kSuccessfulResultCode)
            {
                image = [UIImage imageWithData:data];
            }
            //This means either the session has been expired or has not been set, in any case we need to nullify the session key, because it's invalid
            else if(httpResponse.statusCode == kAuthenticationFailedResultCode)
            {
                DLog(@"Failed to load ad image from backend, The session was expired.");
                weakSelf.sessionKey = nil;
            }
            else
            {
                DLog(@"Failed to load ad from backend. HTTP status code - %ld", (long)httpResponse.statusCode);
            }
        }
        else
        {
            DLog(@"Error occurred when loading ad image from backend %@",connectionError);
            error = connectionError;
        }
        dispatch_sync(dispatch_get_main_queue(), ^{
            callback(image, error);
        });
    }];
}

-(void)registerNewInstallAsync:(RegisterNewInstallCallback)callback
{
    //Create ads url
    NSString *adId = [[[ASIdentifierManager sharedManager] advertisingIdentifier] UUIDString];
    NSURL * registerUrl = [[[NSURL URLWithString:self.baseApiUrl] URLByAppendingPathComponent:kRegisterNewInstallUrl] URLByAppendingPathComponent:adId];
    //Create request
    NSMutableURLRequest * registerRequest = [NSMutableURLRequest requestWithURL:registerUrl cachePolicy:NSURLCacheStorageNotAllowed timeoutInterval:kRequestTimeoutSeconds];
    [registerRequest addValue:self.sessionKey forHTTPHeaderField:kAuthorizationHTTPHeader];
    [registerRequest setHTTPMethod:@"POST"];
    //Get device info and set as body of request
    NSLog(@"REQUEST COUNT: %d", self.requestCount);
    NSDictionary * keyDict = @{ @"extra_key" : [self getRequestKey] };
    NSError * error = nil;
    NSData *jsonKey = [NSJSONSerialization dataWithJSONObject:keyDict options:0 error:&error];
    if(error)
    {
        NSString *JSONString = [[NSString alloc] initWithBytes:[jsonKey bytes] length:[jsonKey length] encoding:NSUTF8StringEncoding];
        DLog(@"Could not convert ALDeviceInfo to json. %@", JSONString);
        dispatch_sync(dispatch_get_main_queue(), ^{
            callback(false, error);
        });
        return;
    }

    [registerRequest setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [registerRequest setValue:@"application/json; charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
    [registerRequest setHTTPBody:jsonKey];
    //Perform request
    __weak typeof(self) weakSelf = self;
    [NSURLConnection sendAsynchronousRequest:registerRequest queue:[NSOperationQueue new] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        BOOL result = false;
        NSError * error = nil;
        if(!connectionError)
        {
            //Convert to http response to get the status code
            NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
            //If successful then get the session key return inside the body
            if(httpResponse.statusCode == kSuccessfulResultCode)
            {
                weakSelf.requestCount++;
                result = true;
            }
            //This means either the session has been expired or has not been set, in any case we need to nullify the session key, because it's invalid
            else if(httpResponse.statusCode == kAuthenticationFailedResultCode)
            {
                DLog(@"Failed to register install. The session was expired.");
                weakSelf.sessionKey = nil;
            }
            //General failure
            else
            {
                result = false;
                DLog(@"Error occurred when registering app to backend. HTTP Status Code - %ld", (long)httpResponse.statusCode);
            }
        }
        else
        {
            result = false;
            error = connectionError;
            DLog(@"Error occurred when registering app to backend. %@",connectionError);
        }
        dispatch_sync(dispatch_get_main_queue(), ^{
            callback(result, error);
        });
    }];
}

-(void)registerNewInstallIfNeeded
{
    if(self.registerNewInstallLocker == nil)
        self.registerNewInstallLocker = [NSObject new];
    __weak typeof(self) weakSelf = self;
    dispatch_queue_t serialQueue = dispatch_queue_create("com.AdLooker.RegisterDeviceQueue", NULL);
    dispatch_async(serialQueue, ^{

        while(true)
        {
            @synchronized(weakSelf.registerNewInstallLocker)
            {
                if(!weakSelf.isRegisterNewInstallInProgress)
                    break;
            }
        }
        
        if(weakSelf.isDeviceRegistered == -1)
        {
            NSUserDefaults * prefs = [NSUserDefaults standardUserDefaults];
            BOOL isDeviceRegistered = [prefs boolForKey:kIsDeviceRegisteredKey];
            if(isDeviceRegistered)
            {
                weakSelf.isDeviceRegistered = 1;
            }
            else
            {
                weakSelf.isDeviceRegistered = 0;
            }
        }
        
        if(weakSelf.isDeviceRegistered == 0)
        {
            @synchronized(weakSelf.registerNewInstallLocker)
            {
                weakSelf.isRegisterNewInstallInProgress = true;
            }
            [weakSelf registerNewInstallAsync:^(BOOL result, NSError * error) {
                if(result == YES)
                {
                    weakSelf.isDeviceRegistered = 1;
                    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kIsDeviceRegisteredKey];
                }
                @synchronized(weakSelf.registerNewInstallLocker)
                {
                    weakSelf.isRegisterNewInstallInProgress = false;
                }
            }];
        }
        
    });
}

-(void)notifyListenersAdLoadedForAdType:(ALAdType)adType loadedAd:(ALAdvertisement *)ad
{
    NSObject * locker = adType == kAdTypeBanner ? self.bannerLocker : self.interestitialLocker;
    @synchronized(locker)
    {
        NSMutableSet * listeners = adType == kAdTypeBanner ? self.bannerListeners : self.interestitialListeners;
        NSMutableArray * nullListeners = [NSMutableArray new];
        __weak typeof(self) weakSelf = self;
        //Validation block
        BOOL (^validate)(id<ALAdListener>) = ^(id<ALAdListener> listener){
            BOOL result = (adType == kAdTypeBanner ? [weakSelf validateBannerListener:listener] : [weakSelf validateInterestitialListener:listener]);
            return result;
        };
        for (WeakListener * adListener in listeners) {
            //Notify if the view is valid and not nil
            if(adListener.weakListener != nil && validate(adListener.weakListener))
            {
                [adListener.weakListener didReloadAd:ad];
            }
            else
            {
                //Add null listeners to separate list
                [nullListeners addObject:adListener];
            }
        }
        //Remove the listeners which are null
        for (WeakListener * adListener in nullListeners) {
            [listeners removeObject:adListener];
        }
    }
}

-(NSString *)getSessionIdFromResponseBody:(NSData *)body
{
    NSError *error = nil;
    NSDictionary *sessionDict = [NSJSONSerialization JSONObjectWithData:body options:NSJSONReadingAllowFragments error:&error];
    
    if(sessionDict == nil || ![[sessionDict allKeys] containsObject:kAuthorizationKey])
    {
        NSString *JSONString = [[NSString alloc] initWithBytes:[body bytes] length:[body length] encoding:NSUTF8StringEncoding];
        DLog(@"Could not parse start session response. %@ %@", error, JSONString);
        return nil;
    }
    
    NSString * sessionID = [sessionDict objectForKey:kAuthorizationKey];
    return sessionID;
}

-(NSString *)getRequestKey
{
    NSString * key = [NSString stringWithFormat:@"%@%ld%@", self.deviceInfo.AdLookerAppId, (long)self.requestCount, self.sessionKey];
    key =  [key MD5];
    return key;
}

#pragma mark - validation
-(BOOL)validateBannerListeners
{
    @synchronized(self.bannerLocker)
    {
        for (WeakListener * listener in self.bannerListeners) {
            if(listener.weakListener != nil)
            {
                //If at least one listener is valid, return true
                if([self validateBannerListener:listener.weakListener])
                {
                    return true;
                }
            }
        }
        return false;
    }
}

-(BOOL)validateInterestitialListeners
{
    @synchronized(self.interestitialLocker)
    {
        for (WeakListener * listener in self.interestitialListeners) {
            if(listener.weakListener != nil)
            {
                //If at least one listener is valid, return true
                if([self validateInterestitialListener:listener.weakListener])
                {
                    return true;
                }
            }
        }
        return false;
    }
}

-(BOOL)validateBannerListener:(id<ALAdListener>)listener
{
    //Validate class
    if(![listener isMemberOfClass:[ALBannerView class]])
        return false;
    
    //Validate common
    ALBannerView * bannerView = (ALBannerView *)listener;
    if(![self validateCommon:bannerView])
        return false;
    
    CGRect expectedRect = [ALAdManager getBannerFrameForPosition:bannerView.position forWindow:bannerView.window];
    
    CGRect frame = bannerView.frame;
    if((int)(frame.size.width+0.5f) != (int)(expectedRect.size.width+0.5f) ||
       (int)(frame.size.height+0.5f) != (int)(expectedRect.size.height+0.5f))
        return false;
    
    //Validate position
    if(bannerView.superview != bannerView.window)
        frame = [bannerView convertRect:bannerView.frame toView:bannerView.window];
    if(bannerView.position != ALBannerPositionFree)
    {
        if((int)(expectedRect.origin.x+0.5f) != (int)(frame.origin.x + 0.5f) ||
           (int)(expectedRect.origin.y+0.5f) != (int)(frame.origin.y+0.5f))
            return false;
    }
    
    return true;
}

-(BOOL)validateInterestitialListener:(id<ALAdListener>)listener
{
    if(![listener isMemberOfClass:[ALInterestitialView class]])
        return false;
    ALInterestitialView * interestitialView = (ALInterestitialView *)listener;
    if(![self validateCommon:interestitialView])
        return false;
    return true;
}

-(BOOL)validateCommon:(UIView *)adView
{
    if(adView.window == nil)
        return false;
    if(adView.alpha != 1)
        return false;
    if(adView.isHidden)
        return false;
    return true;
}

+(CGRect)getBannerFrameForPosition:(ALBannerPosition)position forWindow:(UIWindow *)window
{
    BOOL lessTheniOS8 = [[[UIDevice currentDevice] systemVersion] compare:@"8.0" options:NSNumericSearch] == NSOrderedAscending;
    if( lessTheniOS8 ) {
        return [ALAdManager getBannerFrameForPosition_iOS7:position forWindow:window];
    }
    else {
        return [ALAdManager getBannerFrameForPosition_iOS8:position forWindow:window];
    }
}

+(CGRect)getBannerFrameForPosition_iOS8:(ALBannerPosition)position forWindow:(UIWindow *)window
{
    //Validate size
    CGFloat x=0,y=0,width,height;
    UIDevice * device = [UIDevice currentDevice];
    UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
    BOOL isPortrait = UIInterfaceOrientationIsPortrait(orientation);
    
    if(device.userInterfaceIdiom == UIUserInterfaceIdiomPad)
    {
        if(isPortrait)
            height = BANNER_IPAD_PORTRAIT_HEIGHT;
        else
            height = BANNER_IPAD_LANDSCAPE_HEIGHT;
    }
    else
    {
        if(isPortrait)
            height = BANNER_IPHONE_PORTRAIT_HEIGHT;
        else
            height = BANNER_IPHONE_LANDSCAPE_HEIGHT;
    }
    
    
    CGSize screenSize = window.bounds.size;
    CGFloat statusBarHeight = [ALAdManager getStatusBarHeight];
    BOOL hidden = [UIApplication sharedApplication].statusBarHidden;
    width = screenSize.width;
    
    if(position != ALBannerPositionFree)
    {
        if(position == ALBannerPositionTop)
        {
            y = statusBarHeight;
        }
        else
        {
            y = screenSize.height - height;
        }
    }
    return CGRectMake(x, y, width, height);
}


+(CGRect)getBannerFrameForPosition_iOS7:(ALBannerPosition)position forWindow:(UIWindow *)window
{
    //Validate size
    CGFloat width,height;
    UIDevice * device = [UIDevice currentDevice];
    UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
    BOOL isPortrait = UIInterfaceOrientationIsPortrait(orientation);
    
    if(device.userInterfaceIdiom == UIUserInterfaceIdiomPad)
    {
        if(isPortrait)
            height = BANNER_IPAD_PORTRAIT_HEIGHT;
        else
            height = BANNER_IPAD_LANDSCAPE_HEIGHT;
    }
    else
    {
        if(isPortrait)
            height = BANNER_IPHONE_PORTRAIT_HEIGHT;
        else
            height = BANNER_IPHONE_LANDSCAPE_HEIGHT;
    }
    
    
    CGSize screenSize = window.screen.bounds.size;
    float screenHeight;
    width = isPortrait ? screenSize.width : screenSize.height;
    screenHeight = isPortrait ? screenSize.height : screenSize.width;
    
    CGRect rect =  CGRectMake(0, 0, width, height);
    if(position != ALBannerPositionFree)
    {
        rect = [ALAdManager getSpecificRect:rect orientation:orientation position:position window:window];
    }
    return rect;
}

+ (CGFloat)getStatusBarHeight
{
    if(IS_BELOW_IOS_6())
    {
        return 0;
    }
    else if(IS_IOS_8_OR_HIGHER())
    {
        if([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad)
        {
            return 20;
        }
        else
        {
            UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
            if(UIInterfaceOrientationIsLandscape(orientation))
            {
                return 0;
            }
            else
            {
                return 20;
            }
        }
    }
    else
    {
        UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
        if(UIInterfaceOrientationIsLandscape(orientation))
        {
            return [UIApplication sharedApplication].statusBarFrame.size.width;
        }
        else
        {
            return [UIApplication sharedApplication].statusBarFrame.size.height;
        }
    }
}

+(CGRect)getSpecificRect:(CGRect)rect orientation:(UIInterfaceOrientation)orientation position:(ALBannerPosition)position window:(UIWindow *)window
{
    CGFloat statusBarHeight = [[self class] getStatusBarHeight];
    CGSize screenSize = window.screen.bounds.size;
    CGFloat sW = screenSize.width;
    CGFloat sH = screenSize.height;
    CGFloat W = rect.size.width;
    CGFloat H = rect.size.height;
    if(position == ALBannerPositionTop)
    {
        switch (orientation) {
            case UIInterfaceOrientationLandscapeLeft:
                return CGRectMake(statusBarHeight, (sH-W)/2, H,W);
                break;
            case UIInterfaceOrientationLandscapeRight:
                return CGRectMake(sW-H-statusBarHeight, (sH-W)/2, H,W);
                break;
            case UIInterfaceOrientationPortraitUpsideDown:
                return CGRectMake((sW-W)/2, sH-H-statusBarHeight, W,H);
                break;
            default:
                return CGRectMake((sW-W)/2, statusBarHeight, W,H);
                break;
        }
    }
    else
    {
        switch (orientation) {
            case UIInterfaceOrientationLandscapeLeft:
                return CGRectMake(sW-H, (sH-W)/2, H,W);
                break;
            case UIInterfaceOrientationLandscapeRight:
                return CGRectMake(0, (sH-W)/2, H,W);
                break;
            case UIInterfaceOrientationPortraitUpsideDown:
                return CGRectMake((sW-W)/2, 0, W,H);
                break;
            default:
                return CGRectMake((sW-W)/2, sH-H, W,H);
                break;
        }
    }
    
}

@end
