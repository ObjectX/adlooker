//
//  AdLoader.h
//  AdLooker_iOS
//
//  Created by Gagik Kyurkchyan on 7/29/14.
//  Copyright (c) 2014 com.objectx. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ALAdvertisement.h"
#import "ApiConstants.h"
#import "Constants.h"

@protocol ALAdListener <NSObject>

-(void)didReloadAd:(ALAdvertisement *)newAd;
-(void)hide;

@end

@interface ALAdManager : NSObject

+ (id)instance;
-(void)addAdListener:(id<ALAdListener>)adListener forAdType:(ALAdType)adType;
-(void)removeAdListener:(id<ALAdListener>)adListener forAdType:(ALAdType)adType;

+(CGRect)getBannerFrameForPosition:(ALBannerPosition)position forWindow:(UIWindow *)window;

@end
