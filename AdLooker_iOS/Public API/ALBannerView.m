//
//  ALBannerView.m
//  AdLooker_iOS
//
//  Created by Gagik Kyurkchyan on 7/25/14.
//  Copyright (c) 2014 com.objectx. All rights reserved.
//

#import "ALBannerView.h"
#import "ALAdManager.h"
#import "AGWindowView.h"

#define kWrongInitializationError @"ALBannerView instance can only be initialized using presentTopBanner, presentBottomBanner or initFreeBanner:atPosition: constructor."
#define kNilWindowError @"Application has no key window."

@interface ALBannerView() <ALAdListener>

@property (nonatomic, strong) ALAdvertisement * advertisment;
@property (nonatomic, strong) UIImageView * imageView;
-(void)initializeImageView;
-(void)initializeTapGesture;
-(void)listenOrientationChangeEvents;
-(void)bannerAdTapped:(UITapGestureRecognizer *)tapGesture;

@end

@implementation ALBannerView

static UIView * bannerContainer;
static UIView * topBanner;
static UIView * bottomBanner;
#pragma mark - initialization
+(ALBannerView *)presentTopBanner
{
    ALBannerView * banner = [[ALBannerView alloc] initWithPosition:ALBannerPositionTop];
    return banner;
}

+(ALBannerView *)presentBottomBanner
{
    ALBannerView * banner = [[ALBannerView alloc] initWithPosition:ALBannerPositionBottom];
    return banner;
}

-(void)hide
{
    
    [[ALAdManager instance] removeAdListener:self forAdType:kAdTypeBanner];
    [self removeFromSuperview];
}


-(id)initFreeBanner
{
    return [self initWithPosition:ALBannerPositionFree];
}

-(id)initWithPosition:(ALBannerPosition)position
{
    UIWindow *window = [[UIApplication sharedApplication] keyWindow];
    if(window == nil)
    {
        NSLog(kNilWindowError);
        return nil;
    }
    CGRect frame = [ALAdManager getBannerFrameForPosition:position forWindow:window];
    if(self = [super initWithFrame:frame])
    {
        _position = position;
        [super setBackgroundColor: [UIColor clearColor]];
        [super setFrame:frame];
        [self initializeImageView];
        [self initializeTapGesture];
        if(position != ALBannerPositionFree)
        {
            [window addSubview:self];
            [self updateOrientation:false];
            [self listenOrientationChangeEvents];
        }
    }
    return self;
}

-(void)dealloc
{
    [[ALAdManager instance] removeAdListener:self forAdType:kAdTypeBanner];
}

-(id)initWithCoder:(NSCoder *)aDecoder
{
    NSLog(@"Invalid initialization. %@",kWrongInitializationError);
    return nil;
}

- (id)initWithFrame:(CGRect)frame
{
    NSLog(@"Invalid initialization. %@",kWrongInitializationError);
    return nil;
}

- (id)init
{
    NSLog(@"Invalid initialization.%@", kWrongInitializationError);
    return nil;
}

+(id)new
{
    NSLog(@"Invalid initialization.%@", kWrongInitializationError);
    return nil;
}

-(id)copy
{
    NSLog(@"Invalid operation.%@", kWrongInitializationError);
    return nil;
}

+(id)copyWithZone:(struct _NSZone *)zone
{
    NSLog(@"Invalid operation.%@", kWrongInitializationError);
    return nil;
}

+(id)mutableCopyWithZone:(struct _NSZone *)zone
{
    NSLog(@"Invalid operation.%@", kWrongInitializationError);
    return nil;
}

-(id)mutableCopy
{
    NSLog(@"Invalid operation.%@", kWrongInitializationError);
    return nil;
}

#pragma mark - Invalid Properties
-(void)setFrame:(CGRect)frame
{
    return;
}

-(void)setHidden:(BOOL)hidden
{
    NSLog(@"Invalid operation. %@ of banner view is not allowed.",NSStringFromSelector(_cmd));
}

-(void)setAlpha:(CGFloat)alpha
{
    NSLog(@"Invalid operation. %@ of banner view is not allowed.",NSStringFromSelector(_cmd));
}


-(void)setBackgroundColor:(UIColor *)backgroundColor
{
    NSLog(@"Invalid operation. %@ of banner view is not allowed.",NSStringFromSelector(_cmd));
}

-(void)setOpaque:(BOOL)opaque
{
    NSLog(@"Invalid operation. %@ of banner view is not allowed.",NSStringFromSelector(_cmd));
}

-(void)setTransform:(CGAffineTransform)transform
{
    NSLog(@"Invalid operation. %@ of banner view is not allowed.",NSStringFromSelector(_cmd));
}

-(void)addSubview:(UIView *)view
{
    NSLog(@"Invalid operation. %@ of banner view is not allowed.",NSStringFromSelector(_cmd));
}

-(void)setValue:(id)value forKey:(NSString *)key
{
    NSLog(@"Invalid operation. %@ of banner view is not allowed.",NSStringFromSelector(_cmd));
}

-(void)setValue:(id)value forKeyPath:(NSString *)keyPath
{
    NSLog(@"Invalid operation. %@ of banner view is not allowed.",NSStringFromSelector(_cmd));
}

-(void)setValuesForKeysWithDictionary:(NSDictionary *)keyedValues
{
    NSLog(@"Invalid operation. %@ of banner view is not allowed.",NSStringFromSelector(_cmd));
}

-(void)setBounds:(CGRect)bounds
{
    NSLog(@"Invalid operation. %@ of banner view is not allowed.",NSStringFromSelector(_cmd));
}

-(void)setCenter:(CGPoint)center
{
    NSLog(@"Invalid operation. %@ of banner view is not allowed.",NSStringFromSelector(_cmd));
}

//-(void)setContentScaleFactor:(CGFloat)contentScaleFactor
//{
//    NSLog(@"Invalid operation. %@ of banner view is not allowed.",NSStringFromSelector(_cmd));
//}

-(void)setGestureRecognizers:(NSArray *)gestureRecognizers
{
    NSLog(@"Invalid operation. %@ of banner view is not allowed.",NSStringFromSelector(_cmd));
}

-(void)setNilValueForKey:(NSString *)key
{
    NSLog(@"Invalid operation. %@ of banner view is not allowed.",NSStringFromSelector(_cmd));
}

-(void)setTag:(NSInteger)tag
{
    NSLog(@"Invalid operation. %@ of banner view is not allowed.",NSStringFromSelector(_cmd));
}

#pragma mark - View Lifecycle management

 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect
 {
 // Drawing code
     [super drawRect:rect];
     [[ALAdManager instance] addAdListener:self forAdType:kAdTypeBanner];
 }

#pragma mark - ALAdListener
-(void)didReloadAd:(ALAdvertisement *)newAd
{
    self.advertisment = newAd;
    self.imageView.image = newAd.adImage;
}

#pragma mark - Utility methods

-(void)initializeImageView
{
    self.imageView = [[UIImageView alloc] initWithFrame:self.bounds];
    self.imageView.contentMode = UIViewContentModeScaleAspectFit;
    self.imageView.backgroundColor = [UIColor clearColor];
    [super addSubview:self.imageView];
    self.imageView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    self.imageView.frame = self.bounds;
}

-(void)initializeTapGesture
{
    UITapGestureRecognizer * tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(bannerAdTapped:)];
    [self addGestureRecognizer:tapGesture];
}

-(void)listenOrientationChangeEvents
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(statusBarFrameOrOrientationChanged:) name:UIApplicationDidChangeStatusBarOrientationNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(statusBarFrameOrOrientationChanged:) name:UIApplicationDidChangeStatusBarFrameNotification object:nil];
}

-(void)bannerAdTapped:(UITapGestureRecognizer *)tapGesture
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:self.advertisment.targetUrl]];
}

-(void)statusBarFrameOrOrientationChanged:(NSNotification *)notification
{
    [self updateOrientation:true];
}


-(void)updateOrientation:(BOOL)animated
{
    UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
    CGFloat angle = [self angleForOrientation:orientation];
    CGAffineTransform transform = CGAffineTransformMakeRotation(angle);
    //transform = CGAffineTransformScale(transform, -1, -1);
    CGRect frame = [ALAdManager getBannerFrameForPosition:self.position forWindow:self.window];
    [UIView animateWithDuration: (animated ? 0.1f : 0.0f) animations:^{
        [self setIfNotEqualTransform:transform frame:frame];
    }];

}


- (void)setIfNotEqualTransform:(CGAffineTransform)transform frame:(CGRect)frame
{
    if(!IS_IOS_8_OR_HIGHER() && !CGAffineTransformEqualToTransform(self.transform, transform))
    {
        super.transform = transform;
    }
    if(!CGRectEqualToRect(self.frame, frame))
    {
        super.frame = frame;
    }
}

-(CGFloat)angleForOrientation:(UIInterfaceOrientation)orientation
{
    CGFloat angle;
    switch (orientation) {
        case UIInterfaceOrientationLandscapeLeft:
            angle = -M_PI /2.0;
            break;
            
        case UIInterfaceOrientationLandscapeRight:
            angle = M_PI /2.0;
            break;
            
        case UIInterfaceOrientationPortraitUpsideDown:
            angle = M_PI;
            break;
            
        default:
            angle = 0;
            break;
            
    }
    return  angle;
}

@end
