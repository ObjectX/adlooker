//
//  ALBannerView.h
//  AdLooker_iOS
//
//  Created by Gagik Kyurkchyan on 7/25/14.
//  Copyright (c) 2014 com.objectx. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"
#pragma mark - Constants and enumerations


#pragma mark - AdLooker API

#pragma mark Banner View
/**
 *  ALBannerView class is used to display banners at the top or bottom of your application.
 *  You should avoid manually creating the ALBannerView, rather use presentTopBanner and presentBottomBanner class methods.
 *  You also need to specify base url of the AdLooker server and your AdLooker app id in Info.plist file of your project. Use ADLOOKER_ROOT_URL key for base url and ADLOOKER_APP_KEY for app id.
 */
@class ALBannerView;

@interface ALBannerView : UIView
/**
 *  This method should be used to add ALBannerView to the top of the application.
 *  The created ALBannerView is added to the top of key window of your application.
 *  You should avoid changing the visibility properties and the frame of return ALBannerView. This may cause that the ads won't be loaded.
 *  @return Method returns the created ALBannerView.
 */
+(ALBannerView * )presentTopBanner;
/**
 *  This method should be used to add ALBannerView to the bottom of the application.
 *  The created ALBannerView is added to the bottom of key window of your application.
 *  You should avoid changing the visibility properties and the frame of return ALBannerView. This may cause that the ads won't be loaded.
 *  @return Method returns the created ALBannerView.
 */
+(ALBannerView *)presentBottomBanner;

/**
 *  Removes the ALBannerView from the view hierarchy
 */
-(void)hide;
/**
 *  Returns the position of the ALBannerView
 */
@property (nonatomic, readonly) ALBannerPosition position;

@end
