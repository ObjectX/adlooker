//
//  ALInterestitialViewController.h
//  AdLooker_iOS
//
//  Created by Gagik Kyurkchyan on 8/4/14.
//  Copyright (c) 2014 com.objectx. All rights reserved.
//

#import <UIKit/UIKit.h>
/**
 *   ALInterestitial class is used to display full screen interestitials.
 *  You should avoid manually creating the ALInterestitial, rather use presentInterestitial class method.
 *  You also need to specify base url of the AdLooker server and your AdLooker app id in Info.plist file of your project. Use ADLOOKER_ROOT_URL key for base url and ADLOOKER_APP_KEY for app id.
 */
@interface ALInterestitial : UIView

/**
 *  Will present ALInterestialView at the top of all other views by adding it to your application's key window.
 */
+(void)presentInterestitial;
/**
 *  Will remove ALInterestitialView from view hierarchy.
 */
+(void)closeInterestitial;

@end
