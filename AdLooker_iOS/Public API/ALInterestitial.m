//
//  ALInterestitialViewController.m
//  AdLooker_iOS
//
//  Created by Gagik Kyurkchyan on 8/4/14.
//  Copyright (c) 2014 com.objectx. All rights reserved.
//

#import "ALInterestitial.h"
#import "ALInterestitialView.h"
#import "AGWindowView.h"
#import "ALImages.h"
#import "ALAdManager.h"

@interface ALInterestitial ()

@property (strong, nonatomic) UIView * topBarView;

@property (strong, nonatomic) ALInterestitialView * interestitial;
-(void)addTopBar;
-(void)closeButtonTapped;

@end

@implementation ALInterestitial

static AGWindowView * currentWindow;
static bool isInterestitialShown;
static ALInterestitial * interestitial;
-(id)init
{
    if(self = [super init])
    {
        self.frame = currentWindow.bounds;
        self.backgroundColor = [UIColor whiteColor];
        [self addTopBar];
    }
    return self;
}

#pragma mark - Public API
+(void)presentInterestitial
{
    if(isInterestitialShown)
        return;
    isInterestitialShown = true;
    
    currentWindow = [[AGWindowView alloc] initAndAddToKeyWindow];
    currentWindow.supportedInterfaceOrientations = AGInterfaceOrientationMaskAll;
    interestitial = [[ALInterestitial alloc] init];
    interestitial.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    [currentWindow addSubviewAndFillBounds:interestitial withSlideUpAnimationOnDone:nil];
}

+(void)closeInterestitial
{
    isInterestitialShown = false;
    [currentWindow slideDownSubviewsAndRemoveFromSuperview:^{
        currentWindow = nil;
        [[ALAdManager instance] removeAdListener:interestitial.interestitial forAdType:kAdTypeInterestitial];
        interestitial = nil;
    }];
}

#pragma mark - Utility methods

-(void)addTopBar
{
    self.interestitial = [[ALInterestitialView alloc] init];
    self.interestitial.translatesAutoresizingMaskIntoConstraints = false;
    self.interestitial.backgroundColor = [UIColor whiteColor];
    [self addSubview:self.interestitial];
    
    self.topBarView = [[UIView alloc] init];
    self.topBarView.translatesAutoresizingMaskIntoConstraints = false;
    [self addSubview:self.topBarView];
    self.topBarView.backgroundColor = [UIColor clearColor];
    
    UIButton * closeButton = [[UIButton alloc] init];
    closeButton.translatesAutoresizingMaskIntoConstraints = false;
    [closeButton addTarget:self action:@selector(closeButtonTapped) forControlEvents:UIControlEventTouchUpInside];
    [closeButton setImage:[ALImages getCloseButton] forState:UIControlStateNormal];
    [closeButton setImage:[ALImages getCloseButtonHover] forState:UIControlStateHighlighted];
    [self.topBarView addSubview:closeButton];
    
    UIView * topBarView = self.topBarView;
    UIView * interestitial = self.interestitial;
    NSDictionary * viewDict = NSDictionaryOfVariableBindings(topBarView, closeButton, interestitial);
    NSArray * constraints1 = [NSLayoutConstraint constraintsWithVisualFormat:@"|-0-[topBarView]-0-|" options:0 metrics:nil views:viewDict];
    NSArray * constraints2 = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-20-[topBarView(36)]-0-[interestitial]-0-|" options:0 metrics:nil views:viewDict];
    NSArray * constraints3 = [NSLayoutConstraint constraintsWithVisualFormat:@"[closeButton(20)]-15-|" options:0 metrics:nil views:viewDict];
    NSArray * constraints4 = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[closeButton(20)]" options:0 metrics:nil views:viewDict];
    NSArray * constraints5 = [NSLayoutConstraint constraintsWithVisualFormat:@"|-0-[interestitial]-0-|" options:0 metrics:nil views:viewDict];
    NSArray * constraints6 = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[topBarView]-0-[interestitial]-0-|" options:0 metrics:nil views:viewDict];
    NSLayoutConstraint * constraint7 = [NSLayoutConstraint constraintWithItem:interestitial attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:topBarView attribute:NSLayoutAttributeBottom multiplier:1 constant:0];
    NSLayoutConstraint * vertAlign2 = [NSLayoutConstraint constraintWithItem:closeButton attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:topBarView attribute:NSLayoutAttributeCenterY multiplier:1 constant:0];
    [self addConstraints:constraints1];
    [self addConstraints:constraints2];
    [self.topBarView addConstraints:constraints3];
    [self.topBarView addConstraints:constraints4];
    [self addConstraints:constraints5];
    //[self addConstraints:constraints6];
    //[self addConstraint:constraint7];
//    [self.topBarView addConstraint:vertAlign1];
    [self.topBarView addConstraint:vertAlign2];
    [self layoutIfNeeded];
}

-(void)closeButtonTapped
{
    [ALInterestitial closeInterestitial];
}
@end
