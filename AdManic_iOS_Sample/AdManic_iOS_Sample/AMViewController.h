//
//  AMViewController.h
//  AdLooker_iOS_Sample
//
//  Created by Gagik Kyurkchyan on 8/4/14.
//  Copyright (c) 2014 com.objectx. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AdLooker_iOS/ALBannerView.h>
@interface AMViewController : UIViewController

@property (strong, nonatomic) ALBannerView * topBannerView;
@property (strong, nonatomic) ALBannerView * bottomBannerView;
@end
