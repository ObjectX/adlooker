//
//  main.m
//  AdLooker_iOS_Sample
//
//  Created by Gagik Kyurkchyan on 8/4/14.
//  Copyright (c) 2014 com.objectx. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AMAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AMAppDelegate class]));
    }
}
