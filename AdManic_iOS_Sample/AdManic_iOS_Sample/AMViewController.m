//
//  AMViewController.m
//  AdLooker_iOS_Sample
//
//  Created by Gagik Kyurkchyan on 8/4/14.
//  Copyright (c) 2014 com.objectx. All rights reserved.
//

#import "AMViewController.h"
#import <AdLooker_iOS/ALInterestitial.h>

@interface AMViewController ()

@end

@implementation AMViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    self.view.backgroundColor = [UIColor blueColor];
    float width = 200, height = 50;
    float x = (self.view.frame.size.width - width) / 2;
    float y = (self.view.frame.size.height - height) / 2;
    UIButton * openInterestitial = [[UIButton alloc] initWithFrame:CGRectMake(x, y, width, height)];
    [openInterestitial setTitle:@"Open Interestitial" forState:UIControlStateNormal];
    [openInterestitial setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.view addSubview:openInterestitial];
    [openInterestitial addTarget:self action:@selector(openInterestitial) forControlEvents:UIControlEventTouchUpInside];
    
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    self.topBannerView = [ALBannerView presentTopBanner];
    self.bottomBannerView = [ALBannerView presentBottomBanner];
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)openInterestitial
{
    [ALInterestitial presentInterestitial];
}

@end
