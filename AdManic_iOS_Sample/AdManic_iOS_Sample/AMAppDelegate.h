//
//  AMAppDelegate.h
//  AdLooker_iOS_Sample
//
//  Created by Gagik Kyurkchyan on 8/4/14.
//  Copyright (c) 2014 com.objectx. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AMAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
